package org.artx.transactions.ws;

import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.artx.transactions.model.GetTransactionsResponse;
import org.artx.transactions.model.TransactionResponse;

@Stateless
@WebService(endpointInterface = "org.artx.transactions.ws.TransactionsServiceWS")
public class TransactionsService implements TransactionsServiceWS {

    @PersistenceContext(unitName="transactions")
    private EntityManager entityManager;
    
	@Override
	public String submitResponses(GetTransactionsResponse transactionResponses) {
		
		if (transactionResponses == null || transactionResponses.getTransactions() == null 
				|| transactionResponses.getTransactions().isEmpty()) {
			
			return "Empty";
		}
		
		transactionResponses.getTransactions().forEach(t -> entityManager.persist(TransactionResponse.Factory.fromTransaction(t)));
		
		return "Done";
	}

}
