package org.artx.transactions.ws;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("transactions")
public class SystemInit extends Application {
    
    /**
     * Logger
     */
    private static final Logger logger = Logger.getLogger("SystemInit");
    
    @Override
    public Set<Class<?>> getClasses() {
        return Collections.emptySet();
    }
    
    @Override
    public Set<Object> getSingletons() {
        Set<Object> classes = new HashSet<Object>();
        try {
            InitialContext ctx = new InitialContext();
            Object hasherService = 
              ctx.lookup("java:global/transactions/TransactionServiceWS");
            classes.add(hasherService);
        } catch (Throwable t) {
            logger.log(Level.SEVERE,"Error: ",t);
        }
        return classes;
    }  
}

