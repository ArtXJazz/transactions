package org.artx.transactions.ws;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;

import org.artx.transactions.model.GetTransactionsResponse;

@WebService(targetNamespace = "http://dbo.qulix.com/ukrsibdbo")
@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL, parameterStyle = ParameterStyle.BARE)
public interface TransactionsServiceWS {

	@WebMethod(operationName = "GetTransactionsResponse")
	public String submitResponses(GetTransactionsResponse getTransactionsResponse);

}
