package org.artx.transactions.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "GetTransactionsResponse", namespace = "http://dbo.qulix.com/ukrsibdbo")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetTransactionsResponse implements Serializable {

	private static final long serialVersionUID = 6757333255106888889L;

	@XmlElementWrapper(name = "transactions")
	@XmlElementRef(name = "transaction", namespace = "")
	private List<Transaction> transactions;

	public List<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	} 
	
}
