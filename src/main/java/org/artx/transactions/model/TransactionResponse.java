package org.artx.transactions.model;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="transaction_response")
public class TransactionResponse implements Serializable {

	private static final long serialVersionUID = 6103766267726151459L;
	
	@Id
	@Column(name="ID", insertable=false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="PLACE", nullable=false, length=45)
	private String place;

	@Column(name="AMOUNT", nullable=false)
	private Integer amount;
	
	@Column(name="CURRENCY", nullable=false, length=3)
	private String currency;
	
	@Column(name="CARD_MASKED", nullable=false, length=19)
	private String cardMasked;
	
	@Column(name="CLIENT_FIRST_NAME", nullable=false, length=20)
	private String firstName;
	
	@Column(name="CLIENT_MIDDLE_NAME", nullable=false, length=20)
	private String middleName;
	
	@Column(name="CLIENT_LAST_NAME", nullable=false, length=20)
	private String lastName;
	
	@Column(name="CLIENT_INN", nullable=false, length=10)
	private String inn;

	public Integer getId() {
		return id;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	
	public void setAmount(String amount) {
		Double doubleValue = 0D;
		try {
			doubleValue = new DecimalFormat("###,###,##0.00", new DecimalFormatSymbols(Locale.US)).parse(amount).doubleValue() * 100D;
		} catch (ParseException e) {
			e.printStackTrace();
		} 
		this.amount = (int) Math.round(doubleValue);		
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCardMasked() {
		return cardMasked;
	}

	public void setCardMasked(String cardMasked) {
		this.cardMasked = cardMasked;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getInn() {
		return inn;
	}

	public void setInn(String inn) {
		this.inn = inn;
	}

    public static final class Factory {

    	private Factory() {}
    	
        public static TransactionResponse newInstance()
        {
            return new TransactionResponse();
        }

        public static TransactionResponse fromTransaction(Transaction transaction)
        {
    		TransactionResponse transactionResponse = new TransactionResponse();
    		
    		transactionResponse.setPlace(transaction.getPlace());
    		transactionResponse.setAmount(transaction.getAmount());
    		transactionResponse.setCardMasked(transaction.getCardMasked());
    		transactionResponse.setCurrency(transaction.getCurrency());
    		
    		transactionResponse.setFirstName(transaction.getClient().getFirstName());
    		transactionResponse.setLastName(transaction.getClient().getLastName());
    		transactionResponse.setMiddleName(transaction.getClient().getMiddleName());
    		transactionResponse.setInn(transaction.getClient().getInn());
    		
    		return transactionResponse;
        }

    }
	
}
